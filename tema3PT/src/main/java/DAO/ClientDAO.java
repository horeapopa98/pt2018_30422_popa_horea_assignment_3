package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import Model.User;
import connection.DBConnection;

public class ClientDAO {
	
	public ClientDAO() {
		
	}
	
	public void insertClient(int iD, String username, String password, String cust_name)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("INSERT INTO customer (cust_id,username,password,cust_name) VALUES (?,?,?,?)");
			stmt.setLong(1, iD);
			stmt.setString(2, username);
			stmt.setString(3, password);
			stmt.setString(4, cust_name);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot insert customer into database!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}
	}
	
	public void editClient(String username, String password, String cust_name, int iD)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("UPDATE customer SET username = ?, password = ? ,cust_name = ? WHERE cust_id =?");
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setString(3, cust_name);
			stmt.setLong(4, iD);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot edit customer!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}
	}
	
	public ArrayList<User> getAllUsers()
	{
		Connection connection=DBConnection.getConnection();
		ArrayList<User> users = new ArrayList<User>();
		try {

			Statement stmt;
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM customer");
			
			while(rs.next()) {
				User x =new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				users.add(x);
			}
			return users;
			
		} catch (Exception e) {
			
			JOptionPane optionPane = new JOptionPane("Cannot get customers from database!", JOptionPane.ERROR_MESSAGE);
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);	
			return null;
		}
	}

}
