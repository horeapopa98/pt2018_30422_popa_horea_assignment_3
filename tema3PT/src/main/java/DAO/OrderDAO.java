package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import connection.DBConnection;

public class OrderDAO {
	
	public OrderDAO() {
		
	}
	
	public void insertOrder(int order_id, int prod_id, int quantity)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("INSERT INTO orders (orderID,productID,quantity) VALUES (?,?,?)");
			stmt.setLong(1, order_id);
			stmt.setLong(2, prod_id);
			stmt.setLong(3, quantity);
			stmt.execute();			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot insert order into database!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}	
	}

}
