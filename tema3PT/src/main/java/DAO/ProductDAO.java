package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import Model.Product;
import connection.DBConnection;

public class ProductDAO {
	
	public ProductDAO() {
		
	}
	
	public void insertProduct(int prodID, String prodName, String prodPrice, String prodType, int stock)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("INSERT INTO product (product_id,prod_name,prod_price,prod_type,stock) VALUES (?,?,?,?,?)");
			stmt.setLong(1, prodID);
			stmt.setString(2, prodName);
			stmt.setString(3, prodPrice);
			stmt.setString(4, prodType);
			stmt.setLong(5, stock);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot insert product into database!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}	
	}
	
	public void editProduct(String prodName, String prodPrice, String prodType, int stock, int prodID)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("UPDATE product SET prod_name = ?, prod_price = ?, prod_type = ?, stock = ? WHERE product_id =?");
			stmt.setString(1, prodName);
			stmt.setString(2, prodPrice);
			stmt.setString(3, prodType);
			stmt.setLong(4, stock);
			stmt.setLong(5, prodID);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot edit customer!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}	
	}
	
	public void orderProduct(int stock, int prodID)
	{
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("UPDATE product SET stock = ? WHERE product_id =?");
			stmt.setLong(1, stock);
			stmt.setLong(2, prodID);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot edit customer!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}
	}
		

	public ArrayList<Product> getAllProducts()
	{
		Connection connection=DBConnection.getConnection();
		ArrayList<Product> products = new ArrayList<Product>();
		try {

			Statement stmt;
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM product");
			
			while(rs.next()) {
				Product x =new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
				products.add(x);
			}
			return products;
			
		} catch (Exception e) {
			
			JOptionPane optionPane = new JOptionPane("Cannot get products from database!", JOptionPane.ERROR_MESSAGE);
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);	
			return null;
		}
	}

}
