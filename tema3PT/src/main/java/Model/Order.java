package Model;

public class Order {
	
	private int order_id;
	private int prod_id;
	private int quantity;
	
	public Order(int order_id, int prod_id, int quantity) {
		this.order_id = order_id;
		this.prod_id = prod_id;
		this.quantity = quantity;
	}

	
	public int getOrder_id() {
		return order_id;
	}

	public int getProd_id() {
		return prod_id;
	}

	public int getQuantity() {
		return quantity;
	}
	
}
