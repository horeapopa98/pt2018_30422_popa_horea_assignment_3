package Model;

public class Product {
	private int prodID;
	private String prodName;
	private String prodPrice;
	private String prodType;
	private int stock;
	
	public Product(int prodID, String prodName, String prodPrice, String prodType, int stock) {
		super();
		this.prodID = prodID;
		this.prodName = prodName;
		this.prodPrice = prodPrice;
		this.prodType = prodType;
		this.stock = stock;
	}
	
	public int getProdID() {
		return prodID;
	}

	public String getProdName() {
		return prodName;
	}

	public String getProdPrice() {
		return prodPrice;
	}

	public String getProdType() {
		return prodType;
	}

	public int getStock() {
		return stock;
	}

}
