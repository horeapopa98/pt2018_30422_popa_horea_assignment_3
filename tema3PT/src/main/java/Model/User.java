package Model;

public class User {
	private int ID;
	private String username;
	private String password;
	private String name;
	
	public User(int iD, String username, String password, String name) {
		super();
		ID = iD;
		this.username = username;
		this.password = password;
		this.name = name;
	}

	public int getID() {
		return ID;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

}
