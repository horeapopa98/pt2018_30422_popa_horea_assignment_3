package View;
import javax.swing.*;

import connection.DBConnection;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GUI {
	
	public Map<String,String> customers;
	
	public void getUsers() {
		
		Connection connection=DBConnection.getConnection();
		
		try {
			customers = new HashMap<String,String> ();
			
			Statement stmt;
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT username, password FROM customer");
			
			while(rs.next()) {
				customers.put(rs.getString(1), rs.getString(2));
			}
			
		} catch (Exception e) {
			
			JOptionPane optionPane = new JOptionPane("Cannot get customers from database!", JOptionPane.ERROR_MESSAGE);
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);			
		}
	}
	
	public void insertUser(String username, String password)
	{
		Connection connection=DBConnection.getConnection();
		
		try {
			PreparedStatement stmt;
			stmt = connection.prepareStatement("INSERT INTO customer (username,password) VALUES (?,?)");
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.executeUpdate();
		} catch (Exception e) {
			JOptionPane optionPane = new JOptionPane("Cannot insert customer into database!", JOptionPane.ERROR_MESSAGE);    
			JDialog dialog = optionPane.createDialog("Error");
			dialog.setAlwaysOnTop(true);
			dialog.setVisible(true);
		}
	}
	
	public JFrame frame;
	public JPanel panel1 = new JPanel(null, true);
	public JLabel usernameLabel = new JLabel("username");
	public JTextField usernameTextField = new JTextField();
	public JLabel passwordLabel = new JLabel("password");
	public JPasswordField passwordTextField = new JPasswordField();
	public JButton registerButton = new JButton("register");
	public JButton loginButton = new JButton("login");
	public JLabel errorsLabel = new JLabel("errors");
	
	public void GUI() {
		
		frame = new JFrame();
		frame.setLayout(null);
		frame.setSize(400,400);
		frame.setTitle("Horea's Shop");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel1.setLayout(null);
		panel1.setSize(300, 300);
		panel1.setLocation(50,50);
		
		usernameLabel.setSize(170, 20);
		usernameLabel.setLocation(20, 20);
		panel1.add(usernameLabel);
		
		usernameTextField.setSize(170, 20);
		usernameTextField.setLocation(20, 45);
		panel1.add(usernameTextField);
		
		passwordLabel.setSize(170, 20);
		passwordLabel.setLocation(20, 80);
		panel1.add(passwordLabel);
		
		passwordTextField.setSize(170, 20);
		passwordTextField.setLocation(20, 105);
		panel1.add(passwordTextField);
		
		registerButton.setSize(80, 20);
		registerButton.setLocation(20, 130);
		panel1.add(registerButton);
		
		loginButton.setSize(80, 20);
		loginButton.setLocation(110, 130);
		panel1.add(loginButton);
		
		errorsLabel.setForeground(Color.red);
		errorsLabel.setSize(170, 20);
		errorsLabel.setLocation(20, 0);
		errorsLabel.setVisible(false);

		panel1.add(errorsLabel);
		
		frame.add(panel1);
		frame.setVisible(true);
		
		registerButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				getUsers();
				if(customers.containsKey(usernameTextField.getText())){
					errorsLabel.setText("User exists.");
					errorsLabel.setVisible(true);			
				}
				else
					if (usernameTextField.getText().isEmpty() || passwordTextField.getText().isEmpty())
					{
						errorsLabel.setText("Username and password cannot be empty");
						errorsLabel.setVisible(true);
					}
					else
						insertUser(usernameTextField.getText(),passwordTextField.getText());
			}	 
		});
		
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getUsers();
				if(customers.containsKey(usernameTextField.getText()))  {
					String pass=customers.get(usernameTextField.getText());
					if (pass==null || !pass.equals(passwordTextField.getText()))
					{
						errorsLabel.setText("Invalid password");
						errorsLabel.setVisible(true);
					}
					else
					{
						
						errorsLabel.setVisible(false);
						
						frame.dispose();
						
						secondPage page2 = new secondPage();
						page2.generateTable(page2.client.getAllUsers());
					    page2.introduction();
	 	
					}
				}
				else
				{
					errorsLabel.setText("Invalid username");
					errorsLabel.setVisible(true);
				}
			}
		});
		
		
	}
	
	

}
