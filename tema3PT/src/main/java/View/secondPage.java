package View;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import DAO.ClientDAO;
import DAO.OrderDAO;
import DAO.ProductDAO;
import connection.DBConnection;

public class secondPage {
	
	JTable userTable;
	ClientDAO client = new ClientDAO();
	ProductDAO product = new ProductDAO();
	OrderDAO order = new OrderDAO();
	
	public void introduction() {
		
		final JFrame pg2 = new JFrame();
		pg2.setLayout(new FlowLayout());
		
		final JPanel usersPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		
		final JLabel custIdLabel = new JLabel("Cust ID:");
		final JTextField custIdField = new JTextField();
		final JLabel usernameLabel = new JLabel("username:");
		final JTextField usernameField = new JTextField();
		final JLabel passLabel = new JLabel("password:");
		final JTextField passField = new JTextField();
		final JLabel custNameLabel = new JLabel("Customer name:");
		final JTextField custNameField = new JTextField();
		custIdLabel.setVisible(false);
		custIdField.setVisible(false);
		usernameLabel.setVisible(false);
		usernameField.setVisible(false);
		passLabel.setVisible(false);
		passField.setVisible(false);
		custNameLabel.setVisible(false);
		custNameField.setVisible(false);
		
		final JButton okAddClient = new JButton("OK");
		okAddClient.setVisible(false);
		
		final JLabel prodIdLabel = new JLabel("Prod ID:");
		final JTextField prodIdField = new JTextField();
		final JLabel prodNameLabel = new JLabel("Product name:");
		final JTextField prodNameField = new JTextField();
		final JLabel priceLabel = new JLabel("Price:");
		final JTextField priceField = new JTextField();
		final JLabel prodTypeLabel = new JLabel("Product type:");
		final JTextField prodTypeField = new JTextField();
		final JLabel stockLabel = new JLabel("Stock:");
		final JTextField stockField = new JTextField();
		prodIdLabel.setVisible(false);
		prodIdField.setVisible(false);
		prodNameLabel.setVisible(false);
		prodNameField.setVisible(false);
		priceLabel.setVisible(false);
		priceField.setVisible(false);
		prodTypeLabel.setVisible(false);
		prodTypeField.setVisible(false);
		stockLabel.setVisible(false);
		stockField.setVisible(false);
		
		final JLabel editCustIdLabel = new JLabel("Cust ID:");
		final JTextField editCustIdField = new JTextField();
		final JLabel editUsernameLabel = new JLabel("Edit username:");
		final JTextField editUsernameField = new JTextField();
		final JLabel editPassLabel = new JLabel("Edit password:");
		final JTextField editPassField = new JTextField();
		final JLabel editCustNameLabel = new JLabel("Edit customer name:");
		final JTextField editCustNameField = new JTextField();
		editCustIdLabel.setVisible(false);
		editCustIdField.setVisible(false);
		editUsernameLabel.setVisible(false);
		editUsernameField.setVisible(false);
		editPassLabel.setVisible(false);
		editPassField.setVisible(false);
		editCustNameLabel.setVisible(false);
		editCustNameField.setVisible(false);
		
		final JButton okEditClient = new JButton("OK");
		okEditClient.setVisible(false);
		
		final JLabel editProdIdLabel = new JLabel("Prod ID:");
		final JTextField editProdIdField = new JTextField();
		final JLabel editProdNameLabel = new JLabel("Edit prod name:");
		final JTextField editProdNameField = new JTextField();
		final JLabel editProdPriceLabel = new JLabel("Edit prod price:");
		final JTextField editProdPriceField = new JTextField();
		final JLabel editProdTypeLabel = new JLabel("Edit prod type:");
		final JTextField editProdTypeField = new JTextField();
		final JLabel editStockLabel = new JLabel("Edit stock:");
		final JTextField editStockField = new JTextField();
		editProdIdLabel.setVisible(false);
		editProdIdField.setVisible(false);
		editProdNameLabel.setVisible(false);
		editProdNameField.setVisible(false);
		editProdPriceLabel.setVisible(false);
		editProdPriceField.setVisible(false);
		editProdTypeLabel.setVisible(false);
		editProdTypeField.setVisible(false);
		editStockLabel.setVisible(false);
		editStockField.setVisible(false);
		
		final JButton okEditProduct = new JButton("OK");
		okEditProduct.setVisible(false);		
		
		final JButton okAddProduct = new JButton("OK");
		okAddProduct.setVisible(false);
				
		final JLabel orderLabel = new JLabel("Client ID:");
		final JTextField orderField = new JTextField();
		final JLabel stockOrderLabel = new JLabel("Quantity:");
		final JTextField stockOrderFields = new JTextField();
		final JButton orderButton = new JButton("ORDER");
		final JLabel orderIdLabel = new JLabel("Order ID:");
		final JTextField orderIdField = new JTextField();
		
		JButton viewUsers =new JButton("View all clients");
		viewUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				usersPanel.removeAll();
				
				userTable = generateTable(client.getAllUsers());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
			}});
		
		JButton addUser = new JButton("Add client");
		addUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				custIdLabel.setVisible(true);
				custIdField.setVisible(true);
				usernameLabel.setVisible(true);
				usernameField.setVisible(true);
				passLabel.setVisible(true);
				passField.setVisible(true);
				custNameLabel.setVisible(true);
				custNameField.setVisible(true);
				okAddClient.setVisible(true);
			}	
		});
		
		okAddClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				client.getAllUsers();
				if (custIdField.getText().isEmpty() || usernameField.getText().isEmpty() || passField.getText().isEmpty() || custNameField.getText().isEmpty())
				{
					JOptionPane optionPane = new JOptionPane("Complete all fields", JOptionPane.ERROR_MESSAGE);    
					JDialog dialog = optionPane.createDialog("Error");
					dialog.setAlwaysOnTop(true);
					dialog.setVisible(true);		
				}
				else
					client.insertClient(Integer.parseInt(custIdField.getText()), usernameField.getText(), passField.getText(), custNameField.getText());
				
				usersPanel.removeAll();
				
				userTable = generateTable(client.getAllUsers());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
				
				custIdLabel.setVisible(false);
				custIdField.setVisible(false);
				usernameLabel.setVisible(false);
				usernameField.setVisible(false);
				passLabel.setVisible(false);
				passField.setVisible(false);
				custNameLabel.setVisible(false);
				custNameField.setVisible(false);
				okAddClient.setVisible(false);
				
			}
		});
		
		JButton editUser = new JButton("Edit client");
		editUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editCustIdLabel.setVisible(true);
				editCustIdField.setVisible(true);
				editUsernameLabel.setVisible(true);
				editUsernameField.setVisible(true);
				editPassLabel.setVisible(true);
				editPassField.setVisible(true);
				editCustNameLabel.setVisible(true);
				editCustNameField.setVisible(true);
				okEditClient.setVisible(true);
			}			
		});
		
		okEditClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (userTable==null)
					return;
				int row = userTable.getSelectedRow();
				if (row==-1)
					return;
				int userID = Integer.parseInt(userTable.getModel().getValueAt(row, 0).toString());
				String newUsername = editUsernameField.getText();
				String newPass = editPassField.getText();
				String newCustName = editCustNameField.getText();
				
				client.editClient(newUsername, newPass, newCustName, userID);
				
				usersPanel.removeAll();
				
				userTable = generateTable(client.getAllUsers());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
				
				editCustIdLabel.setVisible(false);
				editCustIdField.setVisible(false);
				editUsernameLabel.setVisible(false);
				editUsernameField.setVisible(false);
				editPassLabel.setVisible(false);
				editPassField.setVisible(false);
				editCustNameLabel.setVisible(false);
				editCustNameField.setVisible(false);
				okEditClient.setVisible(false);
			}			
		});
		
		
		JButton deleteUser = new JButton("Delete client");
		deleteUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (userTable==null)
					return;
				int row = userTable.getSelectedRow();
				if (row==-1)
					return;
				String userID = userTable.getModel().getValueAt(row, 0).toString();

				Connection connection = DBConnection.getConnection();
				PreparedStatement st;
				try {
					st = connection.prepareStatement("DELETE FROM customer WHERE cust_id = ?");
					st.setString(1,userID);
					st.executeUpdate(); 
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				usersPanel.removeAll();
				
				userTable = generateTable(client.getAllUsers());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
			}
		});
		
		JButton addProduct = new JButton("Add product");
		addProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prodIdLabel.setVisible(true);
				prodIdField.setVisible(true);
				prodNameLabel.setVisible(true);
				prodNameField.setVisible(true);
				priceLabel.setVisible(true);
				priceField.setVisible(true);
				prodTypeLabel.setVisible(true);
				prodTypeField.setVisible(true);
				stockLabel.setVisible(true);
				stockField.setVisible(true);
				okAddProduct.setVisible(true);
			}
		});
		
		okAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				product.getAllProducts();
				
				if (prodIdField.getText().isEmpty() || prodNameField.getText().isEmpty() || priceField.getText().isEmpty() || prodTypeField.getText().isEmpty() || stockField.getText().isEmpty())
				{
					JOptionPane optionPane = new JOptionPane("Complete all fields", JOptionPane.ERROR_MESSAGE);    
					JDialog dialog = optionPane.createDialog("Error");
					dialog.setAlwaysOnTop(true);
					dialog.setVisible(true);		
				}
				else
					product.insertProduct(Integer.parseInt(prodIdField.getText()), prodNameField.getText(), priceField.getText(), prodTypeField.getText(), Integer.parseInt(stockField.getText()));
				
				usersPanel.removeAll();
				
				userTable = generateTable(product.getAllProducts());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
				
				prodIdLabel.setVisible(false);
				prodIdField.setVisible(false);
				prodNameLabel.setVisible(false);
				prodNameField.setVisible(false);
				priceLabel.setVisible(false);
				priceField.setVisible(false);
				prodTypeLabel.setVisible(false);
				prodTypeField.setVisible(false);
				stockLabel.setVisible(false);
				stockField.setVisible(false);
				okAddProduct.setVisible(false);				
			}			
		});
		
		JButton editProduct = new JButton("Edit product");
		editProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editProdIdLabel.setVisible(true);
				editProdIdField.setVisible(true);
				editProdNameLabel.setVisible(true);
				editProdNameField.setVisible(true);
				editProdPriceLabel.setVisible(true);
				editProdPriceField.setVisible(true);
				editProdTypeLabel.setVisible(true);
				editProdTypeField.setVisible(true);
				editStockLabel.setVisible(true);
				editStockField.setVisible(true);
				okEditProduct.setVisible(true);
			}	
		});
		
		okEditProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (userTable==null)
					return;
				int row = userTable.getSelectedRow();
				if (row==-1)
					return;
				int prodID = Integer.parseInt(userTable.getModel().getValueAt(row, 0).toString());
				String prodName=editProdNameField.getText();
				String prodPrice=editProdPriceField.getText();
				String prodType=editProdTypeField.getText();
				int prodStock=Integer.parseInt(editStockField.getText());
				
				product.editProduct(prodName, prodPrice, prodType, prodStock, prodID);
				
				usersPanel.removeAll();
				
				userTable = generateTable(product.getAllProducts());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
				
				editProdIdLabel.setVisible(false);
				editProdIdField.setVisible(false);
				editProdNameLabel.setVisible(false);
				editProdNameField.setVisible(false);
				editProdPriceLabel.setVisible(false);
				editProdPriceField.setVisible(false);
				editProdTypeLabel.setVisible(false);
				editProdTypeField.setVisible(false);
				editStockLabel.setVisible(false);
				editStockField.setVisible(false);
				okEditProduct.setVisible(false);			
			}			
		});
		
		JButton deleteProduct = new JButton("Delete product");
		deleteProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (userTable==null)
					return;
				int row = userTable.getSelectedRow();
				if (row==-1)
					return;
				String productID = userTable.getModel().getValueAt(row, 0).toString();

				Connection connection = DBConnection.getConnection();
				PreparedStatement st;
				try {
					st = connection.prepareStatement("DELETE FROM product WHERE product_id = ?");
					st.setString(1,productID);
					st.executeUpdate(); 
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				usersPanel.removeAll();
				
				userTable = generateTable(product.getAllProducts());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
				
			}	
		});

		
		JButton viewProducts = new JButton("View products");
		viewProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				usersPanel.removeAll();
				
				userTable = generateTable(product.getAllProducts());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();
			}		
		});
		
		orderButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (userTable==null)
					return;
				int row = userTable.getSelectedRow();
				if (row==-1)
					return;
				int productID = Integer.parseInt(userTable.getModel().getValueAt(row, 0).toString());
				int stock = Integer.parseInt(userTable.getModel().getValueAt(row, 4).toString());
				
				String prodName = userTable.getModel().getValueAt(row, 1).toString();
				
				int quantity = Integer.parseInt(stockOrderFields.getText());
				int order_id = Integer.parseInt(orderIdField.getText());
				
				if(quantity <= stock)
				{
					product.orderProduct(stock - quantity, productID);
					order.insertOrder(order_id, productID, quantity);
					String filename="Orders.txt";
					try {
						FileWriter fw = new FileWriter(filename,true);
						fw.write("Order number: " + order_id + " client with ID: " + Integer.parseInt(orderField.getText()) + " ordered " + quantity + " pieces of " + prodName);
						fw.write(System.getProperty("line.separator"));
						fw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else
				{
					JOptionPane optionPane = new JOptionPane("Order quantity cannot be higher than the stock!", JOptionPane.ERROR_MESSAGE);    
					JDialog dialog = optionPane.createDialog("Error");
					dialog.setAlwaysOnTop(true);
					dialog.setVisible(true);
				}
				
				usersPanel.removeAll();
				
				userTable = generateTable(product.getAllProducts());
				JScrollPane jsTable = new JScrollPane();
				jsTable=new JScrollPane(userTable);			
				usersPanel.add(jsTable);
				pg2.pack();		
			}			
		});
		
		
		buttonsPanel.setSize(20,100);
		
		buttonsPanel.add(addUser);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(custIdLabel);
		buttonsPanel.add(custIdField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(usernameLabel);
		buttonsPanel.add(usernameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(passLabel);
		buttonsPanel.add(passField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(custNameLabel);
		buttonsPanel.add(custNameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(okAddClient);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editUser);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editCustIdLabel);
		buttonsPanel.add(editCustIdField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editUsernameLabel);
		buttonsPanel.add(editUsernameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editPassLabel);
		buttonsPanel.add(editPassField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editCustNameLabel);
		buttonsPanel.add(editCustNameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(okEditClient);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(deleteUser);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(viewUsers);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));

		buttonsPanel.add(addProduct);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(prodIdLabel);
		buttonsPanel.add(prodIdField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(prodNameLabel);
		buttonsPanel.add(prodNameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(priceLabel);
		buttonsPanel.add(priceField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(prodTypeLabel);
		buttonsPanel.add(prodTypeField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(stockLabel);
		buttonsPanel.add(stockField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(okAddProduct);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editProduct);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editProdIdLabel);
		buttonsPanel.add(editProdIdField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editProdNameLabel);
		buttonsPanel.add(editProdNameField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editProdPriceLabel);
		buttonsPanel.add(editProdPriceField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editProdTypeLabel);
		buttonsPanel.add(editProdTypeField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(editStockLabel);
		buttonsPanel.add(editStockField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(okEditProduct);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(deleteProduct);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(viewProducts);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		
		buttonsPanel.add(orderLabel);
		buttonsPanel.add(orderField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(stockOrderLabel);
		buttonsPanel.add(stockOrderFields);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(orderIdLabel);
		buttonsPanel.add(orderIdField);
		buttonsPanel.add(Box.createRigidArea(new Dimension(0,5)));
		buttonsPanel.add(orderButton);
		

		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
		
		pg2.add(buttonsPanel);
		pg2.add(usersPanel);
		
		pg2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pg2.setTitle("Horea's Shop");
		pg2.setSize(1200,800);
		pg2.setLocation(40, 40);
		pg2.setVisible(true);	
	}
	
	
	public JTable generateTable(ArrayList<? extends Object> list)
	{
		Class c = list.get(0).getClass();
		Field[] fields = c.getDeclaredFields();
		Vector<String> fieldsName =new Vector<String>();
		for (Field f:fields)
			fieldsName.add(f.getName());
		
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		for (Object x:list)
		{
			Vector<String> t = new Vector<String>();
			for (Field f: fields)
				try {
					f.setAccessible(true);
					t.add(f.get(x)+"");
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			content.add(t);
		}
		
		JTable table = new JTable(content,fieldsName);
		
		return table;
	}
}
