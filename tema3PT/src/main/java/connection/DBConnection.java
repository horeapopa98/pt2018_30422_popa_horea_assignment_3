package connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	private static Connection connection;
	
	private DBConnection()
	{
		
	}
	
	private static void connectDatabase(){
		
		String url = "jdbc:mysql://localhost:3306/shop_pt";
		String username = "root";
		String password = "";
		
		System.out.println("Connecting database...");
		
		try  {
			connection = DriverManager.getConnection(url, username, password);
			System.out.println("Database connected!");
		} 
		catch (Exception e) {
			System.out.println("Cannot connect to database!");
		}
	}
	
	public static Connection getConnection()
	{
		if (connection==null)
			connectDatabase();
		return connection;
	}
	
}
